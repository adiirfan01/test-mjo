<?php

namespace Database\Factories;

use App\Models\Merchant;
use App\Models\Outlet;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'outlet_id' => Outlet::factory()->create(),
            'merchant_id' => Merchant::factory()->create(),
            'bill_total' => $this->faker->randomNumber()
        ];
    }
}
