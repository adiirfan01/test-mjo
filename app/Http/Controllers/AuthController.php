<?php

namespace App\Http\Controllers;

use App\Http\Request\AuthRequest;
use App\Models\User;

class AuthController extends Controller
{
    public function login(AuthRequest $request){
        $user = User::where('user_name', $request['username'])
            ->where('password', md5($request['password']))
            ->first();
        if (! $user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $token = auth()->login($user);
        return $this->createNewToken($token);
    }

    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    public function userProfile() {
        return response()->json(auth()->user());
    }

    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
