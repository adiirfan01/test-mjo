<?php

namespace App\Http\Controllers;

use App\Http\Request\LaporanRequest;
use App\Http\Resources\LaporanCollection;
use App\Http\Resources\Paginate;
use App\Models\Merchant;
use App\Models\Outlet;
use App\Repositories\TransactionRepository;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class LaporanController extends Controller
{
    use Paginate;

    public function generateLaporanMerchant(
        Merchant $merchant,
        LaporanRequest $request,
        TransactionRepository $transactionRepo
    )
    {
        $this->authorize('get', $merchant);
        $month = Carbon::create($request->year, $request->month);
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();
        $data = $transactionRepo->queryLaporanByMerchant($merchant);
        $date = collect(CarbonPeriod::create($start,$end)->toArray())->map(function ($date) use ($data, $merchant) {
            $dataByDate = $data->where('date', $date->toDateString())->first();
            return [
                'date' => $date->toDateString(),
                'total' => $dataByDate->total ?? 0,
                'merchantId' => $merchant->id,
                'merchantName' => $merchant->merchant_name
            ];
        });
        $data = new LaporanCollection($this->paginate($date, $request->limit));
        return $data;
    }


    public function generateLaporanOutlet(
        Outlet $outlet,
        LaporanRequest $request,
        TransactionRepository $transactionRepo
    )
    {
        $this->authorize('get', $outlet->merchant);
        $month = Carbon::create($request->year, $request->month);
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();
        $data = $transactionRepo->queryLaporanByOutlet($outlet);
        $merchant = $outlet->merchant;
        $date = collect(CarbonPeriod::create($start,$end)->toArray())->map(function ($date) use ($data, $merchant, $outlet) {
            $dataByDate = $data->where('date', $date->toDateString())->first();
            return [
                'date' => $date->toDateString(),
                'total' => $dataByDate->total ?? 0,
                'merchantId' => $merchant->id,
                'merchantName' => $merchant->merchant_name,
                'outletId' => $outlet->id,
                'outletName' => $outlet->outlet_name
            ];
        });
        $data = new LaporanCollection($this->paginate($date, $request->limit));
        return $data;
    }
}
