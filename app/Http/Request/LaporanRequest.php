<?php

namespace App\Http\Request;


class LaporanRequest extends FormRequest
{
    public function rules(){
        return [
            'year' => ['required'],
            'month' => ['required'],
        ];
    }
}
