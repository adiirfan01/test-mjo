<?php

namespace App\Http\Request;

class AuthRequest extends FormRequest
{
    public function rules(){
        return [
            'username' => ['required'],
            'password' => ['required', 'string'],
        ];
    }
}
