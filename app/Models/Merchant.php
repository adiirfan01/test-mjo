<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    use HasFactory;

    protected $table = 'merchants';

    protected $primaryKey = 'id';

    public function outlet()
    {
        return $this->hasMany(Outlet::class,'merchant_id','id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'merchant_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
