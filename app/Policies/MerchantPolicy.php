<?php

namespace App\Policies;

use App\Models\Merchant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class MerchantPolicy
{
    use HandlesAuthorization;

    public function get(User $user, Merchant $merchant): Response
    {
        if($merchant->user_id == $user->id){
            return $this->allow();
        };

        return $this->deny();
    }
}
