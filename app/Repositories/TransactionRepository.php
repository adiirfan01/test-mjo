<?php

namespace App\Repositories;

use App\Models\Merchant;
use App\Models\Outlet;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionRepository
{
    public function queryLaporanByMerchant(Merchant $merchant)
    {
        return Transaction::select(DB::raw('DATE(created_at) as date'), DB::raw('sum(bill_total) as total'))
            ->where('merchant_id', $merchant->id)
            ->groupBy('date')
            ->get();
    }

    public function queryLaporanByOutlet(Outlet $outlet)
    {
        return Transaction::select(DB::raw('DATE(created_at) as date'), DB::raw('sum(bill_total) as total'))
            ->where('outlet_id', $outlet->id)
            ->groupBy('date')
            ->get();
    }
}
