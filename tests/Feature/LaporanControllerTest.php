<?php

namespace Tests\Feature;

use App\Models\Merchant;
use App\Models\Outlet;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\Helpers\ApiAuth;
use Tests\TestCase;

class LaporanControllerTest extends TestCase
{
    use RefreshDatabase;
    use ApiAuth;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLaporanMerchant()
    {
        $this->actingAsNewUser([
            'id' => 1
        ]);
        $merchant = Merchant::factory()->create([
            'user_id' => 1,
        ]);

        Outlet::factory()->create([
            'merchant_id' => $merchant->id,
            'id' => 1
        ]);

        Transaction::factory()->create([
            'id' => 1,
            'merchant_id' => $merchant->id,
            'outlet_id' => 1,
            'bill_total' => 14000.00,
            'created_at' => Carbon::create('2021-11-01')->timestamp
        ]);

        $response = $this->post('/api/laporan/merchant/'.$merchant->id,[
            'year' => 2021,
            'month' => 11
        ]);
        $responseData = json_decode($response->content());
        $firstData = $responseData->data[0];
        $response->assertStatus(200);
        $this->assertJson(json_encode($firstData), json_encode([
            'date' => '2021-11-01',
            'total' => 14000,
            'merchantId' => 1,
            'merchantName' => $merchant->merchant_name
        ]));
    }

    public function testLaporanOutlet()
    {
        $this->actingAsNewUser([
            'id' => 1
        ]);
        $merchant = Merchant::factory()->create([
            'user_id' => 1,
        ]);

        $outlet = Outlet::factory()->create([
            'merchant_id' => $merchant->id,
            'id' => 1
        ]);

        Transaction::factory()->create([
            'id' => 1,
            'merchant_id' => $merchant->id,
            'outlet_id' => 1,
            'bill_total' => 14000.00,
            'created_at' => Carbon::create('2021-11-01')->timestamp
        ]);

        $response = $this->post('/api/laporan/outlet/'.$outlet->id,[
            'year' => 2021,
            'month' => 11
        ]);
        $responseData = json_decode($response->content());
        $firstData = $responseData->data[0];
        $response->assertStatus(200);
        $this->assertJson(json_encode($firstData), json_encode([
            'date' => '2021-11-01',
            'total' => 14000,
            'merchantId' => $merchant->id,
            'merchantName' => $merchant->merchant_name,
            'outletId' => $outlet->id,
            'outletName' => $outlet->outlet_name
        ]));
    }
}
