<?php

namespace Tests\Helpers;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;

trait ApiAuth
{
    public function actingAsNewUser(array $data = []): User
    {
        $user = User::factory()->create(array_replace([
            'name' => 'admin1',
            'user_name' => 'admin1',
        ], $data));
        $this->actingAs($user, 'api');
        return $user;
    }

    public function actingAs(Authenticatable $user, $driver = null)
    {
        return parent::actingAs($user, $driver ?: 'api');
    }
}
